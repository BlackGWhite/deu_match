const bodyParser = require('body-parser')
const cors = require('cors')

module.exports = (server) => {
    server.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*")
        res.header("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE")
        res.header("Access-Control-Allow-Headers", "Content-Type")
        server.use(cors())
        next()
    })
    server.use(bodyParser.json())
}
