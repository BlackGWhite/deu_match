const { Router } = require('express')
const routes = Router()

routes.post('/deuMatch'), (req, res) => {
    const { nome01 } = req.body
    const { nome02 } = req.body

    const probabilidade = (min = 0, max = 100) => {
        min: number = Math.ceil(min);
        max: number = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

        if (probabilidade < 50) {
            return res.status(200).json({
                "mensagem": `Não foi dessa vez que ${nome01} e ${nome02} ficam juntos`
            })
        }else if (probabilidade >= 50 && probabilidade < 75) {
            return res.status(200).json({
                "mensagem": `O cenario não é perfeito mas ${nome01} e ${nome02} tem boas chances de ficarem juntos`
            })
        }else if (probabilidade > 75) {
            return res.status(200).json({
                "mensagem": `${nome01} e ${nome02} foram feitos um para o outro`
            })
        }

    }

module.exports = router;
