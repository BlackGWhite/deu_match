import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  private readonly URL = "";

  constructor(private http: HttpClient) { }

  porcentagem(nome01: String, nome02: String): Observable<any>{
    const nomeObjeto ={
      nome01: nome01,
      nome02:nome02
    }

    return this.http.post<any>(`${this.URL}/deuMatch`, nomeObjeto)

  }
}
nome01:String;
  nome02: String;